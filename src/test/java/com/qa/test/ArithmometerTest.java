package com.qa.test;

import static org.junit.Assert.assertEquals;

import org.testng.annotations.Test;

public class ArithmometerTest {
	
  @Test
	public void AddAssert() {
		int totalValue = 8;
		int actualValue = 4+4;
		assertEquals("Matches", totalValue, actualValue);
	}

	@Test
	public void MultiplyAssertTrue() {
		int totalValue = 16;
		int actualValue = 4*4;
		assertEquals("Matches", totalValue, actualValue);
	}
}
