package com.qa.test;


import java.io.FileInputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.RemoteWebDriver;

import com.qa.test.util.DriverUtils;


public abstract class BaseTest {
	protected RemoteWebDriver driver;
	private Properties prop = getProperties();

	public BaseTest(String baseUrl) {
		try {
			//String baseUrl = prop.getProperty("baseUrl");
			String browser = prop.getProperty("defaultBrowser");
			
			String hub = (String)prop.getOrDefault("hub", "");
			if(hub.isEmpty()) {
				driver = DriverUtils.getDriver(driver,browser,baseUrl);
			}else {
				driver = DriverUtils.getDriver(driver,hub,browser,baseUrl);
			}
			
			long implicitWaitTimeout = Long.parseLong(prop.getProperty("implicitWaitTimeout"));
			driver.manage().timeouts().implicitlyWait(implicitWaitTimeout, TimeUnit.SECONDS);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public Properties getProperties() {
		// TODO Auto-generated method stub
		InetAddress host = null;
		prop = new Properties();
		try {
			host = InetAddress.getLocalHost();
			if(host.getHostName().startsWith("qa")) {
				prop.load(new FileInputStream("src/main/resources/env-qa.properties"));
			}else {
				prop.load(new FileInputStream("src/main/resources/env-regression.properties"));
			}
		}
			catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		return prop;
		}
		
	}

