package com.qa.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.yaml.snakeyaml.Yaml;


public class DemoLoginTest {
	
  WebDriver driver;
  static final Logger log = LogManager.getLogger(DemoLoginTest.class);
  private static final String YAML_DATA = 
		  			"username: sudipta.mondal@cognizant.test\n" + 
		  			"password: Tosca1234!";
  private static final String YAML_FILE = "C:\\Users\\SONY\\eclipse-workspace\\live-project-parent\\src\\test\\resources\\login-ui.yml";
  
  @BeforeMethod
  public void beforeMethod() {
	  System.setProperty("webdriver.chrome.driver", "E:\\SDET\\chromedriver_win32\\chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
  }
	
  @DataProvider(name = "dp")
  public Object[][] parseYaml() {
	  Yaml yaml = new Yaml();
	  Map<String, String> map = new HashMap<String,String>();
	  map = (Map<String, String>) yaml.load(YAML_DATA);
	  return new Object[][] {map.values().toArray()};
  }
  
  @DataProvider(name = "dp2")
  public Object[][] parseYamlFile() throws FileNotFoundException{
	  Yaml yaml = new Yaml();
	  InputStream iStream = new FileInputStream(new File(YAML_FILE));
	  Collection<Map<String, String>> usersCollection = (Collection<Map<String,String>>)yaml.load(iStream);
	  
	  //declare 2D array to return
	  String[][] users2DArray = new String[usersCollection.size()][2];
	  
	  //parse logic in 2D array
	  int i = 0 ;
	  for(Map<String, String> usersMap: usersCollection) { //Iterate over collection of users
		  Collection userValuesCollection = usersMap.values();
		  for(Object userValue: userValuesCollection) { //Iterate over collection of user values
			  Map<String, String> userMap = (Map<String, String>)userValue;
			  for(Map.Entry<String, String> credentials: userMap.entrySet()) { //Iterate over map of each value
				  if(credentials.getKey().equals("username"))users2DArray[i][0]= credentials.getValue();
				  if(credentials.getKey().equals("password"))users2DArray[i][1]= credentials.getValue();
			  }
		  }
		  i++;
	  }
	  return users2DArray;
  }
  
  @Test(dataProvider = "dp2")
  public void loginTest(String user, String pwd) {
	  log.info("DemoLogin: username" + user + "password" + pwd);
	  WebDriverWait wait = new WebDriverWait(driver, 6);
	  
	  driver.get("http://demowebshop.tricentis.com/");
	  
	  driver.findElement(By.linkText("Log in")).click();
	  
	  driver.findElement(By.id("Email")).sendKeys(user);
	  
	  wait.until(ExpectedConditions.elementToBeClickable(By.id("Password")));
	  driver.findElement(By.id("Password")).sendKeys(pwd);
	  
	  wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@value,'Log')]")));
	  driver.findElement(By.xpath("//input[contains(@value,'Log')]")).click();
	  
	  driver.findElement(By.linkText("Log out")).click();
	  
  }
  

  @AfterMethod
  public void afterMethod() {
	  driver.quit();
  }
  


  
}
