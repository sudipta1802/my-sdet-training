package com.qa.test.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.yaml.snakeyaml.scanner.Constant;

public class DriverUtils {
	
	private static Properties prop;
	private static final String DRIVER_PROP_FILE = "src/main/resources/driver.properties";
	
	public static RemoteWebDriver getDriver(RemoteWebDriver driver, String browser, String baseUrl) 
			throws Exception, IOException, FileNotFoundException {
		prop = new Properties();
		prop.load(new FileInputStream(DRIVER_PROP_FILE));
		if(isWindows()) {
			if(browser.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", prop.getProperty(Constants.CHROME_DRIVER_WIN));
				driver = new ChromeDriver();
			}
			if(browser.equalsIgnoreCase("iexplore")) {
				InternetExplorerOptions options = new InternetExplorerOptions();
				options.introduceFlakinessByIgnoringSecurityDomains();
				options.ignoreZoomSettings();
				
				System.setProperty("webdriver.ie.driver", prop.getProperty(Constants.IE_DRIVER_WIN));
				driver = new InternetExplorerDriver();
			}
		}
		driver.get(baseUrl);
		return driver;
	}
	
	public static RemoteWebDriver getDriver(RemoteWebDriver driver, String hub, String browser, String baseUrl) 
			throws Exception, IOException, FileNotFoundException {
		
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		
		/*
		desiredCapabilities.setBrowserName("Chrome");
		desiredCapabilities.setVersion("71.0");
		//The below code was provided by https://wiki.saucelabs.com/display/DOCS/Platform+Configurator#/
		desiredCapabilities.setCapability("platform", "Windows 10");
		desiredCapabilities.setCapability("version", "71.0");
		desiredCapabilities.setCapability("username", "sudipta2020");
		desiredCapabilities.setCapability("accessKey", "fce95bfd-bcd4-4fda-81a6-f1e002b90c6d");
		desiredCapabilities.setCapability("name", "Saucelab Test by Sudipta");
		*/
		
		
		//Set up when using Saucelabs
		desiredCapabilities.setBrowserName(System.getenv("SELENIUM_BROWSER"));
		desiredCapabilities.setVersion(System.getenv("SELENIUM_VERSION"));
		desiredCapabilities.setCapability("platform", System.getenv("SELENIUM_PLATFORM"));
		desiredCapabilities.setCapability("username", System.getenv("SAUCE_USERNAME"));
		desiredCapabilities.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));
		desiredCapabilities.setCapability("name", "Saucelab Test by Sudipta");
		
		
		driver = new RemoteWebDriver(new URL(hub), desiredCapabilities);
		driver.get(baseUrl);
		return driver;
		
		
		
		/*
		//Below was the code when Saucelabs was NOT used
		 	if(browser.equalsIgnoreCase("chrome")) {
			ChromeOptions options = new ChromeOptions();
			driver = new RemoteWebDriver(new URL(hub), options);
		}
		driver.get(baseUrl);
		return driver;
		*/
	}

	private static boolean isWindows() {
		// TODO Auto-generated method stub
		String os = System.getProperty("os.name");
		return os.startsWith("Windows");
	}

}
